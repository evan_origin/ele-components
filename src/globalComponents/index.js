/**
 * @description 注册全局组件
 * @author Evan Lin (evan_origin@163.com)
 * @date 2020/8/19
 */
import Vue from 'vue'

import BackTop from '../components/backTop/BackTop'
import AccountLogin from '../components/accountLogin/AccountLogin'
import PhoneLogin from '../components/phoneLogin/PhoneLogin'

Vue.component('backTop', BackTop)
Vue.component('accountLogin', AccountLogin)
Vue.component('phoneLogin', PhoneLogin)
